# lab-inventory

Ansible inventory for IOCs to be deployed in the lab.

Each host shall be added to the `hosts` file.
By default variables shall be defined in the `host_vars/<hostname>` file.
Variables used by several IOCs could be defined under `group_vars`.
